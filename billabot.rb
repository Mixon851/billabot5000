require "socket"

$SAFE=1

class Billabot

  def initialize(server, port, nick, channels)
    @server = server
    @port = port
    @nick = nick
    @channels = channels
  end

  def send(s)
    puts "--> #{s}"
    @irc.send "#{s}\n", 0
  end

  def connect()
    @irc = TCPSocket.open(@server, @port)
    send "USER blah blah blah :blah blah"
    send "NICK #{@nick}"
    @channels.each do |x|
      send "JOIN #{x}"
    end
  end

  def evaluate(s)
        if s =~ /^[-+*\/\d\s\eE.()]*$/ then
          begin
            s.untaint
            return eval(s).to_s
            rescue Exception => detail
            puts detail.message()
          end
        end
        return "Error"
  end

  def handle_server_input(s)
    case s.strip
      when /^PING :(.+)$/i
        puts "[ Server ping ]"
        send "PONG :#{$1}"
        send "PRIVMSG #dev :PONG! Server pinged me."
      when /^:(.+?)!(.+?)@(.+?)\sPRIVMSG\s.+\s:[\001]PING (.+)[\001]$/i
        puts "[ CTCP PING from #{$1}!#{$2}@#{$3} ]"
        send "NOTICE #{$1} :\001PING #{$4}\001"
      when /^:(.+?)!(.+?)@(.+?)\sPRIVMSG\s.+\s:[\001]VERSION[\001]$/i
        puts "[ CTCP VERSION from #{$1}!#{$2}@#{$3} ]"
        send "NOTICE #{$1} :\001VERSION Ruby-irc v0.042\001"
      when /^:(.+?)!(.+?)@(.+?)\sPRIVMSG\s(.+)\s:EVAL (.+)$/i
        puts "[ EVAL #{$5} from #{$1}!#{$2}@#{$3} ]"
        send "PRIVMSG #{(($4==@nick)?$1:$4)} :#{evaluate($5)}"
      else
        puts s
    end
  end

  def main_loop()
    @channels.each do |x|
      send "PRIVMSG #{x} :Initializing new Billabot."
      send "PRIVMSG #{x} :Initialization complete. Resistance is futile."
    end
    while true
      ready = select([@irc, $stdin], nil, nil, nil)
      next if !ready
        for s in ready[0]
          if s == $stdin then
            return if $stdin.eof
            s = $stdin.gets
            send s
            elsif s == @irc then
            return if @irc.eof
            s = @irc.gets
            handle_server_input(s)
          end
        end
    end
  end
end

irc = Billabot.new('wilburwebstudios.com', 6667, 'Billabot5000', ['#botoff'])
irc.connect()
begin
  irc.main_loop()
rescue Interrupt
rescue Exception => detail
  puts detail.message()
  print detail.backtrace.join("\n")
  retry
end